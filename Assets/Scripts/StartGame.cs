﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour {
	
	private Controls controller;

	// Use this for initialization
	void Start () {
		controller = GetComponent<Controls> ();
	}
	
	// Update is called once per frame
	void Update () {

		//if the player hits space, start the game.
		if (controller.brake == true)
		{
			Application.LoadLevel ("dyingSquares");
		}
	
	}
}
