﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	public float speed = 10f;
	public float xStop = 0.1f;
	public float yStop = 0.1f;

	public float brakedLinearDrag = 3f;
	public float brakedAngDrag = 1f;

	public float timeFactor;

	public Text playerInfo;

	private Controls controller;
	private Animator animator;

	public GameObject smoke;


	// Use this for initialization
	void Start () {

		controller = GetComponent<Controls> ();
		animator = GetComponent<Animator> ();
	
	}
	
	// Update is called once per frame
	void Update () {

		// reset drag
		rigidbody2D.drag = 0;
		rigidbody2D.angularDrag = 0.1f;

		var forceX = 0f;
		var forceY = 0f;

		// maintain controls no matter framerate
		timeFactor = Time.deltaTime/0.0166f;

		playerInfo.text = timeFactor.ToString();

		forceX = timeFactor * speed * controller.moving.x;
		forceY = timeFactor * speed * controller.moving.y;

		rigidbody2D.AddForce (new Vector2 (forceX, forceY));

		var absVelX = Mathf.Abs(rigidbody2D.velocity.x);
		var absVelY = Mathf.Abs(rigidbody2D.velocity.y);

		// if player is moving very slow, increase drag
		if (absVelX < xStop && absVelY < yStop)
		{
			rigidbody2D.drag = 10;
			
		}
		// if player is using the brake, apply it
		if (controller.brake == true) 
		{
			rigidbody2D.drag = brakedLinearDrag;
			rigidbody2D.angularDrag = brakedAngDrag;
		}
	}
}
