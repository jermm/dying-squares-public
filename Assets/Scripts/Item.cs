﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

	private SpriteRenderer spriteRenderer;

	public int maxStartX = 500;
	public int maxStartY = 500;
	
	public float startHealth = 100;
	public float maxHealth = 100;
	
	public float health;
	public float gainedHealth = 0.0f;

	public float healthLostPerSec = 5;
	public float healthOnPlayerCollision = 25;

	public float healthMultBetweenTwoBoxes = 1.1f;

	public float currentAngVel = 0.0f;
	public float angVelNoHeathLost = 1000f;

	public float t = 0f; //time in sec.
	public float splitTime = 0f;
	public float deadTime = 0f;
	public string nameStr = "Unnamed";

	public bool isAlive = true;

	private Animator animator;

	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
		animator = GetComponent<Animator> ();

		health = startHealth;
		//	give it a push
		rigidbody2D.AddForce (new Vector2(Random.Range (-maxStartX, maxStartX), Random.Range (-maxStartY, maxStartY)));
	}
	
	// Update is called once per frame
	void Update () {

		//get how fast it is spinning.
		currentAngVel = Mathf.Abs(rigidbody2D.angularVelocity);

		float timeChanged = Time.deltaTime;
		t += timeChanged;
		splitTime += timeChanged;

		if (currentAngVel < angVelNoHeathLost)
			health = health - healthLostPerSec * timeChanged;

		// fade it into the backgound.
		if (isAlive) {
			Color start = spriteRenderer.color;
			renderer.material.color = new Color (start.r, start.g, start.b, health / maxHealth);
		}
		// make sure the object dies.
		else
		{
			deadTime += timeChanged;
			if (deadTime > 3.0f)
			{
				Destroy(gameObject);
			}
		}
	}

	void OnCollisionEnter2D(Collision2D target)
	{

		// don't do anything if you aren't alive.
		if (!isAlive)
			return;
			
		animator.SetInteger("AnimState", 2);

		if (target.gameObject.tag == "Player")
		{
			//add to health
			float oldHealth = health;
			health += healthOnPlayerCollision;
			if (health > maxHealth)
				health = maxHealth;

			gainedHealth += health - oldHealth;

		}
		
		if (target.gameObject.tag == "Box") 
		{
			// get the collision target
			Item otherBox = target.gameObject.GetComponent<Item>();

			// dead boxes can't exchange health.
			if (otherBox.isAlive)
			{
				// calc the new heath.
				float rawHealth = (health + otherBox.health) / 2;
				float oldHealth = health;
				health = rawHealth * healthMultBetweenTwoBoxes;

				if (health > maxHealth)
					health = maxHealth;

				otherBox.health = health;

				gainedHealth += health - oldHealth;
			}
		}
	}

	void OnCollisionExit2D(Collision2D target)
	{
		//if the item is alive, animate the collision.
		if(isAlive)
			animator.SetInteger("AnimState", 3);
	}

	public void OnDeath()
	{
		isAlive = false;
		Color start = spriteRenderer.color;
		renderer.material.color =  new Color(start.r, start.g, start.b, .5f);
		animator.SetInteger("AnimState", 1);
	}

	public void end()
	{
		Destroy(gameObject);
	}
}
