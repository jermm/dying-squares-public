﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ItemSpawner : MonoBehaviour {

	public Item firstItem; 
	public List<Item> items;

	public int deadItems = 0;
	public int totalItems = 1;
	public int maxItems = 0;
	public float percentDeathEnd = 0.2f; //20%

	public float timeToAddAnotherObject = 1f;
	public float timeToLeaveDeathClock = 5f;
	
	public float timeBeforeSpliting = 60.0f;

	public float avgHealth = 0.0f;

	private float time = 0f; //time in sec.
	private float timeFromLastObject = 0.0f;
	private float timeToRemoveDeathClock = 0.0f;

	public float timeBeforeStart = 1.0f;

	public Text midScreen;
	public Text bottomScreen;
	public Text topScreen;

	public Points score;
	public float points;

	private int boxNumber = 0;
	private bool enableDeathClock = false;
	public bool isGameOver = false;

	// Use this for initialization
	void Start () {

		items = new List<Item>();

		firstItem.nameStr = "OG.";

		Item clone = Instantiate (firstItem, new Vector3(0,0,0), new Quaternion(0,0,0,0)) as Item;
		items.Add (clone);
		clone.nameStr = boxNumber.ToString();

		boxNumber++;

		midScreen.text = "";
		bottomScreen.text = "";
			
	}
	
	// Update is called once per frame
	void Update () {

		if (isGameOver)
			return;

		time += Time.deltaTime;

		if (time < timeBeforeStart)
			return;

		if (enableDeathClock && time > timeToRemoveDeathClock)
		{			
			midScreen.text = "";
			topScreen.text = "";
			enableDeathClock = false;
		}

		// add another object if 
		if ((time - timeFromLastObject > (timeToAddAnotherObject * items.Count)))
		{ 
			Item clone = Instantiate (firstItem, new Vector3(0,0,0), new Quaternion(0,0,0,0)) as Item;
			items.Add (clone);
			clone.nameStr = boxNumber.ToString();
			boxNumber++;
			timeFromLastObject = time;
			totalItems++;
		}

		int numItems = 0;
		avgHealth = 0;

		foreach(Item t in items.ToArray())
		{
			// remove item from list if health is less than 10
			if (t.health < -10.0f)
			{
				float timeOfDeath = t.t;
				midScreen.text =  timeOfDeath.ToString("F1") + "s";
				midScreen.color = Color.red;
				timeToRemoveDeathClock = time + timeToLeaveDeathClock;
				topScreen.text = t.nameStr;
				points += t.gainedHealth;
				t.OnDeath(); //item will destory itself.
				items.Remove(t);
				enableDeathClock = true;
				deadItems++;
			}
			else
			{
				avgHealth += t.health;
				numItems++;

				// if the item has been alive for more than x sec, spawn a new item, both with a higher decay rate
				if (t.splitTime > timeBeforeSpliting)
				{
					Item clone = Instantiate (firstItem, new Vector3(0,0,0), new Quaternion(0,0,0,0)) as Item;
					clone.nameStr = t.nameStr + ".1";
					clone.healthLostPerSec = t.healthLostPerSec * 2;
					items.Add (clone);
					t.splitTime = 0.0f;
					t.healthLostPerSec = clone.healthLostPerSec;
					totalItems++;
				}
			}
		}
		
		avgHealth = avgHealth / (float)numItems;
		float pAlive = (float)deadItems / (float)totalItems;

		float colorPercentage = (1 - pAlive - percentDeathEnd) / (1 - percentDeathEnd);
		bottomScreen.text = "Alive " + items.Count + " | " + totalItems + " Total"; // + " " + colorPercentage;



		//make bottom more "red" as game ends
		bottomScreen.color = new Color(1, colorPercentage, colorPercentage);

//		Debug.Log ()

		if (maxItems < items.Count)
			maxItems = items.Count;

		if (1 - pAlive < percentDeathEnd)
			onGameOver ();
	}

	void onGameOver()
	{
		isGameOver = true;

		//remove all the items
		foreach (Item t in items.ToArray()) 
		{
			points += t.gainedHealth;
			t.OnDeath(); //item will destory itself.
			items.Remove(t);	
		}

		score.score = points;
		score.time = time;
		score.totalItems = totalItems;
		score.maxItems = maxItems;

		midScreen.text = "";
		bottomScreen.text = "";
		topScreen.text = "";

		score.drawScores = true;
			
	}

	void OnGUI()
	{

		
	}
}
