﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; //list
using UnityEngine.UI; //to write to the UI

public class HelpMessages : MonoBehaviour {

	public List<string> helpStrings;
	public List<float> timeToDisplay;

	public Text topScreen;

	public bool enabled = true;

	private float time;
	public int location = 0;

	// Use this for initialization
	void Start () {

		time = timeToDisplay[0];
		topScreen.text = helpStrings[0];

		if (helpStrings.Count != timeToDisplay.Count) 
		{
			string problemMessage = "helpStrings and timeToDisplay length must match.";
			topScreen.text = problemMessage;
			Debug.Log(problemMessage);
		}

	}
	
	// Update is called once per frame
	void Update () {

		if (!enabled)
			return;

		time = time - Time.deltaTime;

		if (time < 0) 
		{
			location++;

			//if we are at the last message, clear the top.
			if (helpStrings.Count == location)
			{
				//clear message.
				topScreen.text = "";
				enabled = false;
				return;
			}

			topScreen.text = helpStrings[location];
			time = timeToDisplay[location];
		}
	}
}
