﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
//using System.String;
using UnityEngine.UI; //to write to the UI
//using System.Web;
using SimpleJSON;

public class Points : MonoBehaviour {

	public float score = 0.0f;
	public float time = 0.0f;
	public int totalItems = 0;
	public int maxItems = 0;

	public bool drawScores = false;
	public bool runHash = true;
	public Text midScreen;
	public Text bottomScreen;
	public Text topScreen;

	public float timeToWaitBeforeRestart = 1.0f;
	private float gameTime = 0.0f;

	public string hashResults;
	private string key = "ffff";
	private string pName = "";
	private bool nameFound = false;
	private bool waitingForName = false;
	private bool spaceHit = false;

	private Controls controller;

	// Use this for initialization
	void Start () {
		controller = GetComponent<Controls> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (drawScores) 
		{
			gameTime += Time.deltaTime;

			midScreen.color = Color.white;
			midScreen.text = (int)score + "\nAlive: " + maxItems + "\nTime: " + (int)time + "s";
			topScreen.text = "Press <b>Return</b> to restart.";
//			bottomScreen.text = "";

			if (!nameFound && !waitingForName)
			{
				bottomScreen.color = Color.white;
				bottomScreen.text = "Press <b>Space</b> to submit score.";
			}

			if (controller.brake)
				spaceHit = true;

			if (!nameFound && spaceHit)
			{
				topScreen.text = "";
				if (!waitingForName && controller.brake)
				{
					controller.getName = true;
					waitingForName = true;
				}

				bottomScreen.text = "Name: " + controller.playerName + "_";
				topScreen.text = "Press <b>Return</b> to submit.";

				if(!controller.getName)
				{
					nameFound = true;
					pName = controller.playerName;
					gameTime = 0.0f; //reset time.
				}
			}
			else
			{
				//TODO: maybe have more infromation on bottom or mid?
				if (controller.hitReturn && gameTime > timeToWaitBeforeRestart)
				{
					//reload scene.
					Application.LoadLevel ("dyingSquares");
				}

			}

			//when we have the player's name, calc the hash and submit the information.
			if(runHash && nameFound)
				StartCoroutine(submitScore(pName));
		}
	}

	//submits the player's score.
	IEnumerator submitScore(string playerName)
	{
		bottomScreen.text = "Uploading...";

		string rnd = RandomString (4);
		Debug.Log (rnd);

		SHA256 mySHA256 = SHA256Managed.Create ();

		string hashedString = (int)score + "," + maxItems + "," + (int)time + "," + playerName + "," + rnd + key;
		byte[] bytes = Encoding.UTF8.GetBytes (hashedString);
		hashResults = ByteArrayToString(mySHA256.ComputeHash(bytes));

		//make the api call.
		string url = "http://locusgame.net/dying_squares_high_scores/" + (int)score + "/" + maxItems + "/" + (int)time + "/" + playerName + "/" + rnd + "/" + hashResults;

//		url = (url);

		Debug.Log (url);
		WWW www = new WWW(url);

		runHash = false;
		yield return www;

		if (!string.IsNullOrEmpty(www.error)) 
		{
			bottomScreen.text = "Score upload failed.";
			return false;
		}
		
		string results = www.text;
		//TODO: do something with this infromation.
		Debug.Log (results);

		var highScoreResults = JSON.Parse (results);

		string databaseCheck = highScoreResults ["database_status"];
		string apiHealthCheck = highScoreResults ["match"];

		if (databaseCheck.Equals ("true") && apiHealthCheck.Equals ("true")) 
		{
			bottomScreen.text = "Score submitted sucessfully.";
		} 
		else 
		{
			bottomScreen.text = "Issue communicating with server.";
		}

	}

	//converts byte array to hex string
	//from http://stackoverflow.com/a/311179
	public string ByteArrayToString(byte[] ba)
	{
		string hex = System.BitConverter.ToString(ba);
		return hex.Replace("-","");
	}

	//generates a random string.
	//adapted from http://stackoverflow.com/a/1122490
	private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	private string RandomString(int size)
	{
		char[] buffer = new char[size];
		
		for (int i = 0; i < size; i++)
		{
			int rvalue = (int) Random.Range(0.0f, _chars.Length - 1);
			buffer[i] = _chars[rvalue];
		}
		return new string(buffer);
	}
	
}
