﻿using UnityEngine;
using System.Collections;

public class Controls : MonoBehaviour {
	
	public Vector2 moving = new Vector2();

	public string playerName = "";

	public bool brake = false;
	public bool getName = false;
	public bool hitReturn = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (getName) {

			foreach(char c in Input.inputString)
			{

				if (c == "\b"[0] && playerName.Length > 0)
				{
					playerName = playerName.Substring(0, playerName.Length - 1);
				}

				else if (c == "\n"[0] || c == "\r"[0])
				{
					getName = false;
				}

				else if (char.IsLetterOrDigit(c))
				{
					playerName += c;
				}

			}

		}

		brake = false;
		hitReturn = false;
		moving.x = 0;
		moving.y = 0;
		
		if (Input.GetKey("right"))
		{
			moving.x = 1;
		}
		else if (Input.GetKey("left"))
		{
			moving.x = -1;
		}
		
		if (Input.GetKey("up"))
		{
			moving.y = 1;
		}
		
		else if (Input.GetKey("down"))
		{
			moving.y = -1;
		}

		if (Input.GetKey("space"))
		{
			brake = true;
		}

		if (Input.GetKey ("enter") || Input.GetKey ("return")) 
		{
			hitReturn = true;
				}

	}
}
